package com.micehub.base.model;

import java.io.Serializable;

import org.springframework.hateoas.EntityModel;

import lombok.Data;

@Data
public class CompanyModel extends EntityModel<CompanyModel> implements Serializable {
	private static final long serialVersionUID = 8710453830626585754L;
	
	int id;
	int group_id;
	String nameNative;
	String nameEnglish;
	String introNative;
	String introEnglish;
	String country;
}
