package com.micehub.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.micehub.base.entity.GroupApp;

@Repository
@RepositoryRestResource
public interface GroupAppRepository extends JpaRepository<GroupApp, Integer> {

}
