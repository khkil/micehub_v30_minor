package com.micehub.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.micehub.base.entity.User;

@Repository
@RepositoryRestResource
public interface UserRepository extends JpaRepository<User, Integer> {

}
