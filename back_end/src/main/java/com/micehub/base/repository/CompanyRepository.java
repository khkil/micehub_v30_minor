package com.micehub.base.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.micehub.base.entity.Company;

@Repository
@RepositoryRestResource
public interface CompanyRepository extends JpaRepository<Company, Integer> {

	Page<Company> findByGroup_id(final int groupId, Pageable pageable);
	
	Page<Company> findByNameEnglishIgnoreCaseContainsOrNameNativeIgnoreCaseContains(
			final String nameEnglish, final String nameNative, Pageable pageable);
}
