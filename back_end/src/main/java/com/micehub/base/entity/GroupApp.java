package com.micehub.base.entity;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@ToString(exclude = {"registeredCompany"})
@Table(catalog = "micehub_v30")
@EqualsAndHashCode(exclude = {"group", "registeredCompany"})
public class GroupApp {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	String name;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JsonIgnoreProperties("apps")
	Group group;
	
	@ManyToMany(mappedBy = "joinedApp")
	@JsonIgnoreProperties("joinedApp")
	List<Company> registeredCompany;

}
