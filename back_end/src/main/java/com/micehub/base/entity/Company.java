package com.micehub.base.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@ToString(exclude = "group")
@Table(catalog = "micehub_v30")
@EqualsAndHashCode(exclude = {"group", "joinedApp"})
public class Company {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "group_id")
	@JsonIgnoreProperties({"users", "apps"})
	Group group;
	
	@Column(name = "name_native")
	String nameNative;
	
	@Column(name = "name_english")
	String nameEnglish;
	
	String country;

	@Column(name = "intro_native")
	String introNative;
	
	@Column(name = "intro_english")
	String introEnglish;
	
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		catalog = "micehub_v30",
		name = "group_app_company", 
		joinColumns = @JoinColumn(name="company_id"),
		inverseJoinColumns = @JoinColumn(name="app_id"))
	@JsonIgnoreProperties("registeredCompany")
	List<GroupApp> joinedApp;
	
	
}
