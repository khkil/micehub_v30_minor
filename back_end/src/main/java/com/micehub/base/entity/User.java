package com.micehub.base.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(catalog = "micehub_v30")
@EqualsAndHashCode(exclude = {"group"})
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	String username;
	String password;
	
	String name;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "group_id")
	@JsonIgnoreProperties("users")
	Group group;
	
	

}
