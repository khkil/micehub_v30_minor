package com.micehub.base;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.micehub.base.repository.CompanyRepository;
import com.micehub.base.service.ElasticsearchService;
import com.micehub.base.service.ElasticsearchServiceImpl;

@SpringBootApplication
@Controller
public class MicehubBaseApplication implements CommandLineRunner {
	
	@Autowired CompanyRepository companyRepository;

	@Autowired
	@Qualifier("elasticsearchRestHighLevelClient")
	RestHighLevelClient client;
	
	public static void main(String[] args) {
		SpringApplication.run(MicehubBaseApplication.class, args);
		
	}
	
	@Bean
	@Scope("prototype")
	public ElasticsearchService elasticsearchService() {
		return new ElasticsearchServiceImpl();
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
	
	
	@GetMapping("/")
	public ModelAndView main(ModelAndView mv) {
		
		mv.addObject("who", "World");
		mv.setViewName("main");
		
		return mv;
	}

	@Override
	public void run(String... args) throws Exception {
		/*
		List<Company> companyList = companyRepository.findAll();
		for(Company company : companyList) {
			
		}
		*/
	}
}
