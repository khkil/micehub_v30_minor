package com.micehub.base.service;

import java.io.IOException;
import java.util.Map;

import org.springframework.boot.configurationprocessor.json.JSONObject;

public interface ElasticsearchService {
	
	public void index(String indexName, String documentType, String message) throws IOException;
	
	public Map<String, Object> getIndex(String index, String type, String id);

	public JSONObject search(String index, String field, String text);

}
