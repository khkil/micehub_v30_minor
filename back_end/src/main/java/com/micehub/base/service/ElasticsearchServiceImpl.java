package com.micehub.base.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.catalina.connector.Response;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.script.mustache.SearchTemplateRequest;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

@Service("elasticsearchService")
public class ElasticsearchServiceImpl implements ElasticsearchService {
	
	Logger logger = LoggerFactory.getLogger(ElasticsearchServiceImpl.class);

	@Autowired
	@Qualifier("elasticsearchRestHighLevelClient")
	RestHighLevelClient client;
	
	
	@Override
	public void index(String indexName, String documentType, String jsonMessage) {
		IndexRequest request = new IndexRequest(indexName)
				.type(documentType)
				.source(jsonMessage, XContentType.JSON);
		
		try {
			IndexResponse response = client.index(request, RequestOptions.DEFAULT);
			logger.info("Response: "+
					Strings.toString(response.toXContent(XContentFactory.jsonBuilder().prettyPrint(), ToXContent.EMPTY_PARAMS)));
			
		} catch(ElasticsearchException e) {
			logger.error(e.getMessage(), e);
			
		} catch(IOException e) {
			logger.error(e.getMessage(), e);
			
		}
	}


	@Override
	public Map<String, Object> getIndex(String index, String type, String id) {

		GetRequest reqeust = new GetRequest(index, type, id);
		GetResponse response = null;
		Map<String, Object> sourceAsMap = new HashMap<String, Object>();
		
		try {
			response = client.get(reqeust, RequestOptions.DEFAULT);
			sourceAsMap = response.getSourceAsMap();
			
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			
		}
		return sourceAsMap;
	}



	@Override
	public JSONObject search(String index, String field, String text) {

		SearchRequest searchRequest = new SearchRequest(index);
		QueryBuilder queryBuilder = QueryBuilders.matchQuery(field, text);
		SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
		sourceBuilder.query(queryBuilder);
		searchRequest.source(sourceBuilder);
		
		SearchResponse searchResponse = null;
		JSONObject result = new JSONObject();
		try {
			searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
			result = new JSONObject(searchResponse.toString());
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} catch (JSONException e) {
			result = new JSONObject();
			e.printStackTrace();
		}
		
		return result;
	}

}
