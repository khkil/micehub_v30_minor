package com.micehub.base.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.micehub.base.entity.User;
import com.micehub.base.repository.UserRepository;

@RestController
@RequestMapping("/rest")
public class TestController {

	@Autowired
	UserRepository userRepository;
	
	@GetMapping("/users")
	public List<User> users() {
		return userRepository.findAll();
	}
}
