package com.micehub.base.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.micehub.base.entity.Company;
import com.micehub.base.entity.Group;
import com.micehub.base.repository.CompanyRepository;
import com.micehub.base.service.ElasticsearchService;

@Controller
@RequestMapping("/admin/company")
public class AdminCompanyController {

	/**
	 * TODO: Make Service Layer
	 */
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	@Qualifier("elasticsearchService")
	ElasticsearchService esService;
	
	/**
	 * 관리자 - 관리 회사 목록
	 * @param model
	 * @param pageable
	 * @return
	 */
	@RequestMapping
	public String list(Model model, @PageableDefault(size = 20, sort = "nameNative") Pageable pageable) {
		
		model.addAttribute("companyList", companyRepository.findByGroup_id(getDummyGroup().getId(), pageable));

		return "admin/company/list";
	}

	/**
	 * 관리자 - 관리 회사 추가
	 * @param model
	 * @return
	 */
	@RequestMapping("/create")
	public String insertPage(Model model) {
		
		model.addAttribute("company", (Company) null);
		model.addAttribute("group", getDummyGroup());
		
		return "admin/company/edit";
	}

	/**
	 * 관리자 - 관리 회사 편집
	 * @param companyId
	 * @param model
	 * @return
	 */
	@RequestMapping("/{companyId}/edit")
	public String modifyPage(@PathVariable int companyId, Model model) {
		
		Company company = companyRepository.getOne(companyId);
		model.addAttribute("company", company);
		model.addAttribute("group", company.getGroup());
		
		return "admin/company/edit";
	}
	
	/**
	 * 관리자 -관리 회사 편집 > 저장
	 * @param company
	 * @param model
	 * @return
	 */
	@PostMapping("/save")
	public String save(Company company, Model model) {

		try {
			company = companyRepository.save(company);
			
			esService.index("history", "_doc", objectMapper.writeValueAsString(company));
			
		}catch(Exception e) {
			model.addAttribute("error", e.getMessage());
			return company.getId() == 0 ? insertPage(model) : modifyPage(company.getId(), model);
		}
		
		return "redirect:/admin/company/edit/"+company.getId();
	}
	
	/**
	 * 테스트용 그룹
	 * @return
	 */
	private Group getDummyGroup() {
		Group group = new Group();
		group.setId(1);
		group.setName("Micehub");
		return group;
	}
	
}
