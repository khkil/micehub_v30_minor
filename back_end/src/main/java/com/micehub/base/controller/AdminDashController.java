package com.micehub.base.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminDashController {

	@GetMapping("/admin")
	public String main() {
		
		return "admin/index";
	}
}
