package com.micehub.base.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminLoginController {

	@GetMapping(path = "/admin/login")
	public String loginForm() {
		return "admin/login";
	}
}
