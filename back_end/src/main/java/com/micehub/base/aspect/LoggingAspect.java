package com.micehub.base.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
	Logger logger = LoggerFactory.getLogger(LoggingAspect.class);
	
	@Around("execution(* *..*Repository.*(..))")
	public Object log(ProceedingJoinPoint jp) throws Throwable {
		logger.info("#start - "+jp.getSignature());
		try {
			Object result = jp.proceed();

			logger.info("#target");
			logger.info(jp.getTarget().toString());
			logger.info("#this");
			logger.info(jp.getThis().toString());
			logger.info("#args");
			for(Object arg : jp.getArgs()) {
				logger.info(arg.toString());
			}
			

			logger.info("#ok - "+jp.getSignature());
			return result;
			
		}catch(Exception e) {
			logger.error(e.getMessage(), e);
			logger.info("#error - "+jp.getSignature());
			throw e;
			
		}finally {
			logger.info("#end - "+jp.getSignature());
		}
		
	}
	
}
