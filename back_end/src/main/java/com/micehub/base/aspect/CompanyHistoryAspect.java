package com.micehub.base.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.MDC.MDCCloseable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.micehub.base.entity.Company;

@Aspect
@Component
public class CompanyHistoryAspect {
	Logger logger = LoggerFactory.getLogger("com.micehub.logger.HISTORY");
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Around("execution(* com.micehub.base.repository.CompanyRepository.*(..))"
			+ "&& (execution(* *..*.save(..))"
				+ "|| execution(* *..*.saveAndFlush(..))"
				+ "|| execution(* *..*.delete(..)) )")
	public Object logHistory(ProceedingJoinPoint jp) throws Throwable {
		try {
			Object result = jp.proceed();
			String companyJson = objectMapper.writeValueAsString(result);
			logger.trace(companyJson);
			return result;
		}catch(Exception e) {
			throw e;
		}
	}
}
