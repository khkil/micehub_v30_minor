package com.micehub.base;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;

@Configuration
public class WebMvcConfig {
	
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
	    configurer.favorPathExtension(false).
		    favorParameter(false).
		    parameterName("mediaType").
		    ignoreAcceptHeader(false).
		    defaultContentType(MediaType.TEXT_HTML).
		    mediaType("json", MediaType.APPLICATION_JSON); 
	}
}
