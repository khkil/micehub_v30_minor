package com.micehub.base;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.CALLS_REAL_METHODS;

import java.io.IOException;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.micehub.base.service.ElasticsearchService;

//@RunWith(SpringRunner.class)
@SpringBootTest
class ElasticApplicationTest {
	
	@Autowired
    private ElasticsearchService service;
    private final String INDEX_NAME = "history";
    private final String TYPE_NAME = "_doc";
    private final String FIELD_NAME = "nameEnglish";
	
    /*
    @Before
    public void connection_start() {
        client = new RestHighLevelClient(RestClient.builder( new HttpHost("127.0.0.1", 9200, "http")));
    }
    @After
    public void connection_exit() throws IOException {
        client.close();
    }
    */
	@Test
	void get_test() throws IOException {
		
    	Map<String, Object> indexMap = service.getIndex(INDEX_NAME, TYPE_NAME, "x8KaP3IBBUCYkVJHBSsc");
    	
		System.out.println("indexMap: "+indexMap.toString());
		
	}
	
	@Test
	void search() throws IOException, JSONException {
		
		boolean result = false;
		String searchText = "acefair11";
		JSONObject searchResult = service.search(INDEX_NAME, FIELD_NAME, searchText);
		JSONArray hits = searchResult.getJSONObject("hits").getJSONArray("hits");
		System.out.println("searchResult: "+searchResult.toString());
		
		if(hits.length() > 0) {
			result = true;
		}
		assertTrue(result);
	}
}